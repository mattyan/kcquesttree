# -*- coding: utf-8 -*-
'''
Created on 2016/12/14

@author: mattyan
'''
import pyquery
import re
import json
import os
import urllib

class QuestType:
	'''
		任務種別
	'''
	Daily       = 0b00000000000001 # デイリー
	Weekly      = 0b00000000000010 # ウィークリー
	Monthly     = 0b00000000000100 # モンスリー
	Quarterly   = 0b00000000001000 # 季節
	Single      = 0b00000000010000 # 単体
	Othor       = 0b00000000100000 # その他

	Formation   = 0b00000001000000 # 編成(Axx)
	Battle      = 0b00000010000000 # 戦闘(Bxx)
	Exercises   = 0b00000100000000 # 演習(Cxx)
	Expedition  = 0b00001000000000 # 遠征(Dxx)
	Supply      = 0b00010000000000 # 補給/入渠(Exx)
	Arsenal     = 0b00100000000000 # 工廠(Fx)
	Remodelling = 0b01000000000000 # 改装(Gxx)
	Wedding     = 0b10000000000000 # ケッコンカッコカリ(WFxx)

	@classmethod
	def get_type_name(cls, quest_type):
		'''
			任務種別の文字列情報の取得
			@param quest_type: 任務種別の論理和
			@return 任務種別の文字列
		'''
		type_name = ''
		if quest_type & QuestType.Arsenal:
			type_name = '工廠'
		elif quest_type & QuestType.Battle:
			type_name = '出撃'
		elif quest_type & QuestType.Exercises:
			type_name = '演習'
		elif quest_type & QuestType.Expedition:
			type_name = '遠征'
		elif quest_type & QuestType.Formation:
			type_name = '編成'
		elif quest_type & QuestType.Supply:
			type_name = '補給'
		elif quest_type & QuestType.Remodelling:
			type_name = '改装'

		if quest_type & QuestType.Daily:
			type_name += '(日)'
		elif quest_type & QuestType.Weekly:
			type_name += '(週)'
		elif quest_type & QuestType.Monthly:
			type_name += '(月)'
		elif quest_type & QuestType.Quarterly:
			type_name += '(季節)'
		elif quest_type & QuestType.Othor:
			type_name += '(他)'
		elif quest_type & QuestType.Single:
			type_name += '(単)'
		return type_name

class Node:
	'''
		任務ノード管理クラス
	'''
	def __init__(self, update):
		if not os.path.exists('lib/quest_node.json') or update:
			request = urllib.request.Request('http://wikiwiki.jp/kancolle/?%C7%A4%CC%B3', data = None, headers = {'User-Agent': 'kcquesttree'})
			html = urllib.request.urlopen(request).read()
			self.quest_tree = self.__parse(html)

			quest_node_json = json.dumps(self.quest_tree, ensure_ascii = False)
			file = open('lib/quest_node.json', 'w', encoding = 'utf-8')
			file.write(quest_node_json)
			file.close()
		else:
			file = open('lib/quest_node.json', 'r', encoding = 'utf-8')
			quest_node_json = file.read()
			file.close()
			self.quest_tree = json.loads(quest_node_json)

	def __parse(self, html):
		'''
			艦これWikiページから任務情報取得
			@param html: 任務一覧ページのhtml
			@return: 任務ノード
		'''
		quests = {}
		nodes = pyquery.PyQuery(html).find('.ie5 table tbody tr')
		conditions = [] # 前回条件
		condition = ''
		tmp_children = {}
		quest_ids = []
		up_id = '' # ↑達成後用のID
		pattern = re.compile(r'\(([A-Z][a-zA-Z]?\d+)\)')
		id_pattern = re.compile(r'[A-Z][a-zA-Z]?\d+')
		up_pattern = re.compile(r'\s*↑\s*')
		for node in nodes.items():
			tds = node.find('td')
			quest_id = tds.eq(0).text()

			if re.match(id_pattern, quest_id):
				if len(tds) >= 9:
					if not re.match(r'^\d{4}\s*\d*/?\d*$', tds.eq(8).text()):
						condition = tds.eq(8).text()
				conditions = []
				if re.search(up_pattern, condition):
					if len(tds) >= 8:
						up_id = quest_ids[-1]
					conditions.append(up_id)
					if up_id in quests:
						quests[up_id]['children'].append(quest_id)
					else:
						if up_id in tmp_children:
							tmp_children[up_id].append(quest_id)
						else:
							tmp_children.update({up_id: [quest_id]})
				else:
					for c in re.finditer(pattern, condition):
						conditions.append(c.group(1))

						if c.group(1) in quests:
							quests[c.group(1)]['children'].append(quest_id)
						else:
							if c.group(1) in tmp_children:
								tmp_children[c.group(1)].append(quest_id)
							else:
								tmp_children.update({c.group(1): [quest_id]})
				quest_ids.append(quest_id)

				quests.update({quest_id: {
					'id': quest_id,
					'name': tds.eq(1).text(),
					'conditions': conditions,
					'root': True if len(conditions) == 0 else False,
					'type': self.__get_quest_type(quest_id, condition),
					'children': [] if not quest_id in tmp_children else tmp_children[quest_id]
				}})

		return quests

	def __get_quest_type(self, quest_id, append):
		'''
			任務IDから種別を取得
			@param quest_id: 任務ID
			@param append: 追加情報
			@return: 任務種別の論理和
		'''
		quest_type = 0
		quest_key = quest_id[0]
		if quest_key == 'A':
			# 編成任務
			quest_type |= QuestType.Formation
		elif quest_key == 'B':
			# 戦闘任務
			quest_type |= QuestType.Battle
			quest_subkey = quest_id[1]
			if quest_subkey == 'd':
				quest_type |= QuestType.Daily
			elif quest_subkey == 'w':
				quest_type |= QuestType.Weekly
			elif quest_subkey == 'm':
				quest_type |= QuestType.Monthly
			elif quest_subkey == 'q':
				quest_type |= QuestType.Quarterly
		elif quest_key == 'C':
			# 演習任務
			quest_type |= QuestType.Exercises
		elif quest_key == 'D':
			# 遠征任務
			quest_type |= QuestType.Expedition
		elif quest_key == 'E':
			# 補給/入渠任務
			quest_type |= QuestType.Supply
		elif quest_key == 'F':
			# 工廠任務
			quest_type |= QuestType.Arsenal
		elif quest_key == 'G':
			# 改装任務
			quest_type |= QuestType.Remodelling
		elif quest_key == 'N':
			# SN作戦任務専用(SNxxx)
			pass
		elif quest_key == 'R':
			# 礼号作戦任務専用ID(ORxxx)
			if quest_id[1] == '1':
				quest_type |= QuestType.Battle
			elif quest_id[1] == '2':
				quest_type |= QuestType.Expedition
		elif quest_key == 'O':
			if quest_id[1] == 'H':
				if quest_id[2] == '1':
					quest_type |= QuestType.Arsenal
				elif quest_id[2] == '2':
					quest_type |= QuestType.Arsenal
				elif quest_id[2] == '3':
					quest_type |= QuestType.Battle

		else:
			# その他
			return self.__get_quest_type(quest_id[1:], append)
		if 'デイリー' in append:
			quest_type |= QuestType.Daily
		elif 'ウィークリー' in append:
			quest_type |= QuestType.Weekly
		elif 'マンスリー' in append:
			quest_type |= QuestType.Monthly
		elif 'クォータリー' in append:
			quest_type |= QuestType.Quarterly
		else:
			quest_type |= QuestType.Single

		return quest_type

	def get_node(self, quest_id):
		return self.quest_tree[quest_id]

	def get_node_list(self):
		return self.quest_tree

