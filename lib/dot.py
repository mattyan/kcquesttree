# -*- coding: utf-8 -*-
'''
Created on 2016/12/16

@author: mattyan
'''

from lib.node import QuestType

class Dot:
	'''
		Dotファイルの作成
	'''
	def get_dot(self, tree):
		'''
			dotデータの作成
			@param tree: 任務ツリー
			@return: dotデータ
		'''
		dot = '''
digraph kancolle_quest_tree {
	graph [
		charset = "UTF-8";
		layout= dot;
	]
	node [
		shape = record
		fontname="sansa"
		style = "solid,filled"
	]

'''
		# ノードの登録
		registed_node = []
		root_nodes = []
		for node in tree['node']:
			if not node['id'] in registed_node:

				fillcolor = ''

				if node['type'] & QuestType.Arsenal:
					fillcolor = 'brown'
				elif node['type'] & QuestType.Battle:
					fillcolor = 'tomato'
				elif node['type'] & QuestType.Exercises:
					fillcolor = 'limegreen'
				elif node['type'] & QuestType.Expedition:
					fillcolor = 'cornflowerblue'
				elif node['type'] & QuestType.Formation:
					fillcolor = 'forestgreen'
				elif node['type'] & QuestType.Remodelling:
					fillcolor = 'limegreen'
				elif node['type'] & QuestType.Supply:
					fillcolor = 'khaki'

				dot += '''	{0} [
		label = "{0} | {{ {1} | {{ 種別 | {2} }} }}",
		fillcolor = "{3}"
	];
'''.format(node['id'], node['name'], QuestType.get_type_name(node['type']), fillcolor)
				registed_node.append(node['id'])
				if node['root']:
					root_nodes.append(node)

		# エッジの登録
		registed_edge = []
		for edge in tree['edge']:
			if not '{0}->{1}'.format(edge['parent'], edge['child']) in registed_edge:
				dot += '	{0} -> {1}\n'.format(edge['parent'], edge['child'])
				registed_edge.append('{0}->{1}'.format(edge['parent'], edge['child']))
		# ルートノードを同じ位置に
		if len(root_nodes) > 0:
			dot += '	{rank = same; '
			for node in root_nodes:
				dot += node['id'] + '; '
			dot += '}\n'
		dot += '}'
		return dot

	def write_file(self, path, tree):
		'''
			ファイルの書き出し
			@param path: dotファイルのパス
			@param tree: 任務ツリー
		'''
		dot = self.get_dot(tree)
		file = open(path, 'w', encoding = 'utf-8')
		file.write(dot)
		file.close()

		return
