# KCQuestTree
## 概要
[艦これ攻略Wikiの任務一覧ページ](http://wikiwiki.jp/kancolle/?%C7%A4%CC%B3)の情報を基にクエストツリーのdotファイルの作成

### 必須モジュール
- pyquery
- graphviz(モジュールではないけど、png作成に必要な外部プログラム)

## インストール

### モジュールのインストール
- pip3 install pyquery
- [graphviz](http://www.graphviz.org/)からダウンロードしてインストール

### 本体のpull
git clone https://gitlab.com/mattyan/kcquesttree.git


## 実行方法
- python3 kcquesttree.py
- tree.pngが作成されるのでgraphvizをインストールしてdot -Tpng tree.dot -oOUTPUT.png

### usage
kcquesttree.py \[-h\] \[--from FROM_QUEST_ID\] \[--to TO_QUEST_ID\] \[--dot DOT\] \[--update\]

optional arguments:
-  -h, --help            show this help message and exit
-  --from FROM_QUEST_ID  任務ID: この任務からの依存ツリーを作成
-  --to TO_QUEST_ID      任務ID: この任務につながる依存ツリーを作成
-  --dot DOT             dotファイルのパス
-  --update              任務キャッシュの更新
