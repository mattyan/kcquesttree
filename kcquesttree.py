# -*- coding: utf-8 -*-
'''
Created on 2016/12/14
艦これWikiの任務一覧ページから任務ツリー作成
@author: mattyan
'''

import argparse

from lib.node import Node
from lib.tree import Tree
from lib.dot import Dot

if __name__ == '__main__':
	# コマンドライン引数の解析
	arg_parser = argparse.ArgumentParser(description='艦これ攻略Wikiの任務一覧ページの情報を基にクエストツリーのdotファイルの作成')
	arg_parser.add_argument('--from', dest = 'from_quest_id', help = '任務ID: この任務からの依存ツリーを作成')
	arg_parser.add_argument('--to', dest = 'to_quest_id', help = '任務ID: この任務につながる依存ツリーを作成')
	arg_parser.add_argument('--dot', dest = 'dot', help = 'dotファイルのパス', default = 'tree.dot')
	arg_parser.add_argument('--update', help = '任務キャッシュの更新', const = True, default = False, action = 'store_const')
	args = arg_parser.parse_args()

	quest_node = Node(args.update)
	quest_tree = Tree(quest_node)
	if args.from_quest_id is not None:
		tree = quest_tree.get_tree(args.from_quest_id, Tree.Tree_Type_From)
	elif args.to_quest_id is not None:
		tree = quest_tree.get_tree(args.to_quest_id, Tree.Tree_Type_To)
	else:
		tree = quest_tree.get_all_tree()
	# dotファイル生成
	Dot().write_file(args.dot, tree)
