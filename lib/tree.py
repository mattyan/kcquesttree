# -*- coding: utf-8 -*-
'''
Created on 2016/12/15

@author: mattyan
'''

class Tree:
	'''
		任務ツリーの作成管理クラス
	'''
	node = None
	nodes = []
	edges = []

	Tree_Type_To = 'conditions'
	Tree_Type_From = 'children'
	def __init__(self, node):
		self.node = node

	def __parse_tree(self, quest_id, condition_type):
		'''
			親子関係の解析
			@param quest_id: 任務ID
			@param condition_type: 依存方向(FromかToか)
		'''
		node = self.node.get_node(quest_id)
		self.nodes.append(node)
		if len(node[condition_type]) == 0:
			return
		else:
			for condition in node[condition_type]:
				self.__parse_tree(condition, condition_type)
				if condition_type == self.Tree_Type_To:
					self.edges.append({'child': node['id'], 'parent': condition})
				else:
					self.edges.append({'child': condition, 'parent': node['id']})

	def get_tree(self, quest_id, tree_type = Tree_Type_To):
		'''
			任務ツリーの一部を作成
			@param quest_id: 起点か終点の任務ID
			@param tree_type: quest_idからのツリーか、quest_idへのツリーか
			@return: 任務ツリー
		'''
		self.nodes = []
		self.edges = []
		self.__parse_tree(quest_id, tree_type)
		return {'node': self.nodes, 'edge': self.edges}

	def get_all_tree(self):
		'''
			任務ツリーを作成
			@return: 任務ツリー
		'''
		self.nodes = []
		self.edges = []
		for node in self.node.get_node_list().values():
			if node['root']:
				self.__parse_tree(node['id'], self.Tree_Type_From)

		return {'node': self.nodes, 'edge': self.edges}